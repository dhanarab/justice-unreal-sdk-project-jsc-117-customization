// Copyright (c) 2018 - 2021 AccelByte Inc. All Rights Reserved.
// This is licensed software from AccelByte Inc, for limitations
// and restrictions contact your company contract manager.

#include "AccelByteUe4SdkCustomizationModule.h"

class FAccelByteUe4SdkCustomizationModule : public IAccelByteUe4SdkCustomizationModuleInterface
{
    virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};

void FAccelByteUe4SdkCustomizationModule::StartupModule()
{

}

void FAccelByteUe4SdkCustomizationModule::ShutdownModule()
{
	
}

IMPLEMENT_MODULE(FAccelByteUe4SdkCustomizationModule, AccelByteUe4SdkCustomization)
