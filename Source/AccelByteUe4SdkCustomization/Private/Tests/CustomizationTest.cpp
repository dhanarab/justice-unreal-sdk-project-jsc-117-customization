// Copyright (c) 2021 AccelByte Inc. All Rights Reserved.
// This is licensed software from AccelByte Inc, for limitations
// and restrictions contact your company contract manager.

#include "Misc/AutomationTest.h"

#include "TestUtilities.h"
#include "Core/AccelByteMultiRegistry.h"
#include "Core/AccelByteError.h"
#include "Models/AccelByteCustomizationModels.h"
#include "Api/AccelByteCustomizationApi.h"

DECLARE_LOG_CATEGORY_EXTERN(LogAccelByteCustomizationTest, Log, All);
DEFINE_LOG_CATEGORY(LogAccelByteCustomizationTest);

static const int32 AutomationFlagMaskCustomization = (EAutomationTestFlags::EditorContext | EAutomationTestFlags::ProductFilter | EAutomationTestFlags::CommandletContext | EAutomationTestFlags::ClientContext);

IMPLEMENT_COMPLEX_AUTOMATION_TEST(DoUStructTest, "AccelByte.Tests.Customization.DoUStruct", AutomationFlagMaskCustomization)
void DoUStructTest::GetTests(TArray<FString>& OutBeautifiedNames, TArray <FString>& OutTestCommands) const
{
	OutBeautifiedNames.Add("GET");
	OutTestCommands.Add("GET");
	OutBeautifiedNames.Add("POST");
	OutTestCommands.Add("POST");
	OutBeautifiedNames.Add("PUT");
	OutTestCommands.Add("PUT");
	OutBeautifiedNames.Add("DELETE");
	OutTestCommands.Add("DELETE");
}

bool DoUStructTest::RunTest(const FString& Method)
{
	FApiClient ApiClient;

	ApiClient.Credentials.SetClientCredentials(FRegistry::Settings.ClientId, FRegistry::Settings.ClientSecret);

	AB_TEST_TRUE(SetupTestUser(ApiClient));

	AccelByte::Api::Customization CustomizationApi = ApiClient.GetApi<AccelByte::Api::Customization>();

	FCustomizationAnythingResponse Result;

	bool bIsDone = false;
	bool bIsSuccess = false;

	FString Query = TEXT("=123");

	FCustomizationAnythingRequest Content;

	Content.Name = "John Doe";
	Content.Age = 24;

	CustomizationApi.DoUStruct(Method, Query, Content,
		THandler<FCustomizationAnythingResponse>::CreateLambda([&](const FCustomizationAnythingResponse& Response)
			{
				Result = Response;
				bIsSuccess = true;
				bIsDone = true;
			}),
		FErrorHandler::CreateLambda([&](const int32 Code, const FString& Message)
			{
				bIsDone = true;
			}));

	Waiting(bIsDone, "Waiting ...");

	AB_TEST_TRUE(bIsSuccess);
	AB_TEST_EQUAL(Result.Method, Method);
	AB_TEST_EQUAL(Result.Args["q"], Query);
	AB_TEST_EQUAL(Result.Headers[TEXT("Authorization")], FString::Printf(TEXT("Bearer %s"), *ApiClient.Credentials.GetAccessToken()));
	if (!Method.ToUpper().Equals(TEXT("GET")))
	{
		AB_TEST_EQUAL(Result.Json.Name, Content.Name);
		AB_TEST_EQUAL(Result.Json.Age, Content.Age);
	}

	AB_TEST_TRUE(TearDownTestUser(ApiClient));

	return bIsSuccess;
}

IMPLEMENT_COMPLEX_AUTOMATION_TEST(DoFormTest, "AccelByte.Tests.Customization.DoForm", AutomationFlagMaskCustomization)
void DoFormTest::GetTests(TArray<FString>& OutBeautifiedNames, TArray <FString>& OutTestCommands) const
{
	OutBeautifiedNames.Add("GET");
	OutTestCommands.Add("GET");
	OutBeautifiedNames.Add("POST");
	OutTestCommands.Add("POST");
	OutBeautifiedNames.Add("PUT");
	OutTestCommands.Add("PUT");
	OutBeautifiedNames.Add("DELETE");
	OutTestCommands.Add("DELETE");
}

bool DoFormTest::RunTest(const FString& Method)
{
	FApiClient ApiClient;

	ApiClient.Credentials.SetClientCredentials(FRegistry::Settings.ClientId, FRegistry::Settings.ClientSecret);

	AB_TEST_TRUE(SetupTestUser(ApiClient));

	AccelByte::Api::Customization CustomizationApi = ApiClient.GetApi<AccelByte::Api::Customization>();

	FCustomizationAnythingResponse Result;

	bool bIsDone = false;
	bool bIsSuccess = false;

	FString Query = TEXT("=123");

	TMap<FString, FString> Content = {
		{"name", "John Doe"},
		{"age", "24"},
	};

	CustomizationApi.DoForm(Method, Query, Content,
		THandler<FCustomizationAnythingResponse>::CreateLambda([&](const FCustomizationAnythingResponse& Response)
			{
				Result = Response;
				bIsSuccess = true;
				bIsDone = true;
			}),
		FErrorHandler::CreateLambda([&](const int32 Code, const FString& Message)
			{
				bIsDone = true;
			}));

	Waiting(bIsDone, "Waiting ...");

	AB_TEST_TRUE(bIsSuccess);
	AB_TEST_EQUAL(Result.Method, Method);
	AB_TEST_EQUAL(Result.Args["q"], Query);
	AB_TEST_EQUAL(Result.Headers[TEXT("Authorization")], FString::Printf(TEXT("Bearer %s"), *ApiClient.Credentials.GetAccessToken()));
	if (!Method.ToUpper().Equals(TEXT("GET")))
	{
		AB_TEST_EQUAL(Result.Form[TEXT("name")], TEXT("John Doe"));
		AB_TEST_EQUAL(Result.Form[TEXT("age")], TEXT("24"));
	}

	AB_TEST_TRUE(TearDownTestUser(ApiClient));

	return bIsSuccess;
}
