// Copyright (c) 2021 AccelByte Inc. All Rights Reserved.
// This is licensed software from AccelByte Inc, for limitations
// and restrictions contact your company contract manager.

#include "Api/AccelByteCustomizationApi.h"

#include "Core/AccelByteReport.h"

namespace AccelByte
{
	namespace Api
	{
		Customization::Customization(Credentials const& CredentialsRef, Settings const& SettingsRef, FHttpRetryScheduler& HttpRef) :
			HttpRef{ HttpRef },
			CredentialsRef{ CredentialsRef },
			SettingsRef{ SettingsRef },
			HttpClient(CredentialsRef, SettingsRef, HttpRef)
		{}

		Customization::~Customization() {}

		void Customization::DoUStruct(const FString& Verb, const FString& Query, const FCustomizationAnythingRequest& Content, const THandler<FCustomizationAnythingResponse> OnSuccess, const FErrorHandler& OnError)
		{
			FReport::Log(FString(__FUNCTION__));

			HttpClient.ApiRequest(Verb, TEXT("https://httpbin.org/anything"), { { "q", Query } },
				Content, OnSuccess, OnError);
		}

		void Customization::DoForm(const FString& Verb, const FString& Query, const TMap<FString,FString>& Content, const THandler<FCustomizationAnythingResponse> OnSuccess, const FErrorHandler& OnError)
		{
			FReport::Log(FString(__FUNCTION__));

			HttpClient.ApiRequest(Verb, TEXT("https://httpbin.org/anything"), { { "q", Query } },
				Content, OnSuccess, OnError);
		}
	}
}
