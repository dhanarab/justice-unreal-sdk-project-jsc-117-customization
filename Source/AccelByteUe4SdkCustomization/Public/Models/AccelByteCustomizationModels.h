// Copyright (c) 2020 AccelByte Inc. All Rights Reserved.
// This is licensed software from AccelByte Inc, for limitations
// and restrictions contact your company contract manager.

#pragma once

#include "CoreMinimal.h"
//#include "Models/AccelByteGeneralModels.h"
#include "AccelByteCustomizationModels.generated.h"

USTRUCT()
struct ACCELBYTEUE4SDKCUSTOMIZATION_API FCustomizationAnythingRequest
{
	GENERATED_BODY()
	UPROPERTY()
		FString Name;
	UPROPERTY()
		int32 Age;
};

USTRUCT()
struct ACCELBYTEUE4SDKCUSTOMIZATION_API FCustomizationAnythingResponse
{
	GENERATED_BODY()
	UPROPERTY()
		FString Method;
	UPROPERTY()
		TMap<FString, FString> Args;
	UPROPERTY()
		TMap<FString, FString> Headers;
	UPROPERTY()
		TMap<FString, FString> Form;
	UPROPERTY()
		FCustomizationAnythingRequest Json;
};
