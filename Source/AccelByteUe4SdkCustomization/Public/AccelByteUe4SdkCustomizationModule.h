// Copyright (c) 2018 - 2019 AccelByte Inc. All Rights Reserved.
// This is licensed software from AccelByte Inc, for limitations
// and restrictions contact your company contract manager.

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"

class IAccelByteUe4SdkCustomizationModuleInterface : public IModuleInterface
{
public:
	static IAccelByteUe4SdkCustomizationModuleInterface& Get()
	{
		return FModuleManager::LoadModuleChecked<IAccelByteUe4SdkCustomizationModuleInterface>("AccelByteUe4SdkCustomization");
	}

	static bool IsAvailable()
	{
		return FModuleManager::Get().IsModuleLoaded("AccelByteUe4SdkCustomization");
	}
};
