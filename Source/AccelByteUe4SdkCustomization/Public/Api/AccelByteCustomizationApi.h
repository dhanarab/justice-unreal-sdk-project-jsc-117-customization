// Copyright (c) 2021 AccelByte Inc. All Rights Reserved.
// This is licensed software from AccelByte Inc, for limitations
// and restrictions contact your company contract manager.

#pragma once

#include "Core/AccelByteError.h"
#include "Core/AccelByteMultiRegistry.h"
#include "Core/AccelByteHttpRetryScheduler.h"
#include "Core/AccelByteHttpClient.h"
#include "Models/AccelByteCustomizationModels.h"

namespace AccelByte
{
	class Credentials;
	class Settings;
	class FHttpRetryScheduler;
	class FHttpClient;

	namespace Api
	{
		class ACCELBYTEUE4SDKCUSTOMIZATION_API Customization : FApiBase
		{
		public:
			Customization(Credentials const& CredentialsRef, Settings const& SettingsRef, FHttpRetryScheduler& HttpRef);
			~Customization();

			void DoUStruct(const FString& Method, const FString& Query, const FCustomizationAnythingRequest& Content, const THandler<FCustomizationAnythingResponse> OnSuccess, const FErrorHandler& OnError);
			void DoForm(const FString& Method, const FString& Query, const TMap<FString,FString>& Content, const THandler<FCustomizationAnythingResponse> OnSuccess, const FErrorHandler& OnError);

		private:
			FHttpRetryScheduler& HttpRef;
			Credentials const& CredentialsRef;
			Settings const& SettingsRef;
			FHttpClient HttpClient;
		};

	}
}
